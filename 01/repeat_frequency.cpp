#include <iostream>
#include <fstream>
#include <unordered_set>
using namespace std;

int main()
{
	int result = 0;
	int read = 0;
	unordered_set<int> set;

	ifstream input;
	while(1)
	{
		input.open("input.txt");

		while (input)
		{
			input >> read;
			if (read == 0) break;
			//cout <<"read:"<< read << endl;
			result += read;
			//cout << "result:" << result << endl;
            if(set.find(result) == set.end())
            {
              set.insert(result);
            }
            else
            {
                std::cout << result << endl;
                return 0;
            }
			read = 0;


		}
        input.close();
	}
	std::cout << result << endl;
	return 0;
}
